import datetime
from datetime import date
import string
import random
import json
import os.path


def generate_data_file(semester):
    """
    Function for generating a random data file to 
    be used in analysis
    """
    TODAY = datetime.datetime.today().strftime('%d/%m/%Y')
    output = {
        'date': TODAY,
        'semester': semester,
        'data': {}
    }

    def random_eval(nb_evals):
        stats = {}
        for i in range(1, 10):
            pp = random.randint(0, nb_evals)
            p = random.randint(0, nb_evals - pp)
            m = random.randint(0, nb_evals - pp - p)
            mm = nb_evals - pp - p - m
            stats[str(i)] = {
                '++': pp,
                '+': p,
                '-': m,
                '--': mm
            }
        return stats

    def random_date():
        start_date = date.today().toordinal() - 60
        end_date = date.today().toordinal()
        random_day = date.fromordinal(random.randint(start_date, end_date))
        return random_day.strftime('%d/%m/%Y')

    def random_string(l=40):
        return ''.join(random.choices(string.ascii_uppercase, k=l))

    NB_UVS = 200
    for uv in range(1, NB_UVS):
        uv_code = 'UV' + str(uv)
        nb_etu_registered = random.randint(10, 100)
        nb_etu_abs = random.randint(1, 9)
        nb_etu_passed = random.randint(1, nb_etu_registered - nb_etu_abs)
        nb_evals = random.randint(1, nb_etu_registered - nb_etu_abs)
        date_review_teacher = random.choice([None, random_date()])
        date_review_conseil = random.choice([None, random_date()])
        teacher_comment = None
        conseil_comment = None
        if date_review_teacher:
            teacher_comment = random_string(random.randint(0, 500))
        if date_review_conseil:
            conseil_comment = random_string(random.randint(0, 500))

        output['data'][uv_code] = {
            'name': random_string(),
            'nb_etu_registered': nb_etu_registered,
            'nb_etu_abs': nb_etu_abs,
            'nb_etu_passed': nb_etu_passed,
            'nb_evals': nb_evals,
            'teacher_name': random_string(10),
            'stats': random_eval(nb_evals),
            'date_review_teacher': date_review_teacher,
            'teacher_comment': teacher_comment,
            'date_review_conseil': date_review_conseil,
            'conseil_comment': conseil_comment
        }

    fp = os.path.realpath(__file__) + '/../../data/' + semester + '.json'
    with open(os.path.abspath(fp), 'w') as outfile:
        json.dump(output, outfile)
