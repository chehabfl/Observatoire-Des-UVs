import socket


def is_connected(debug):
    hostname = "www.google.fr"
    try:
        host = socket.gethostbyname(hostname)
        s = socket.create_connection((host, 80), 2)
        return True
    except:  # noqa: E722
        pass
    print("No connection detected")
    return False


def to_mardown_table_str(table):

    def array_to_str(arr):
        res = ''
        for s in arr:
            res += s
        return res

    def build_line(arr):
        return ['|'] + [s + '|' for s in arr] + ["\n"]

    str_tmp = build_line(table[0])
    str_tmp += build_line(['--' for s in table[0]])
    for i in range(1, len(table)):
        str_tmp += build_line(table[i])

    return array_to_str(str_tmp)
