Observatoire des évaluations d'UVs
====
[![build](/../badges/master/build.svg)](https://gitlab.utc.fr/chehabfl/Observatoire-Des-UVs/pipelines) 
[![License](https://img.shields.io/badge/License-BSD%202--Clause-green.svg)](https://opensource.org/licenses/BSD-2-Clause)

# Objectif


- Donner un aperçu général des résultats des évaluations d'UVs ;
- Proposer des visualisations temporelles des évaluations d'UVs.



# Remarque sur les données


## Mais t'es où ? Pas là !


**Les données des évaluations d'UVs ne sont pas mises à disposition dans ce repo.**


Des données aléatoires peuvent être générées (dans le fichier `config.yml`, mettre `allow_generate: true`) si vous souhaitez contribuer.


Concernant l'intégration continue, les (vraies) données sont récupérées depuis un autre repo.


## Format


Les données de chaque semestre doivent être au format `json` en respectant la structure suivante :


```json
{
    "date":"14/05/2018",
    "semester":"A2017",
    "data":{
        "UVXX":{
            "name":"Intitulé de l'UV",
            "nb_etu_registered":28,
            "nb_etu_abs":0,
            "nb_etu_passed":24,
            "nb_evals":28,
            "stats":{
                "1":{"++":19,"+":8,"-":1,"--":0},
                "2":{"++":21,"+":7,"-":0,"--":0},
                "3":{"++":8,"+":10,"-":8,"--":2},
                "4":{"++":16,"+":11,"-":1,"--":0},
                "5":{"++":21,"+":5,"-":1,"--":1},
                "6":{"++":21,"+":6,"-":1,"--":0},
                "7":{"++":21,"+":6,"-":1,"--":0},
                "8":{"++":15,"+":9,"-":3,"--":1},
                "9":{"++":9,"+":12,"-":6,"--":1},
                "10":{"++":15,"+":11,"-":2,"--":0}
            },
            "teacher_name":"Nom du responsable",
            "teacher_comment":"comentaire ou null (sans les quotes)",
            "date_review_teacher":"jj/mm/yyyy ou null(dans les quotes)",
            "conseil_comment":"comentaire ou null (sans les quote)",
            "date_review_conseil":"jj/mm/yyyy ou null(dans les quotes)"
        },
        "UVXY":{}
    }
}
```
Ces données doivent être stockées dans un dossier `data` à la racine du projet.



# Technologies

- Python 3
- Jupyter Notebook
- JavaScript (pour plus de fonctionnalités)
- (HTML / css / Bootstrap)


# Installation

Sur Linux :

```bash
git clone https://gitlab.utc.fr/chehabfl/Observatoire-Des-UVs.git
cd Observatoire-Des-UVs
virtualenv -p /usr/bin/python3 proj_env        
source activate proj_env/bin/activate
pip install -r ./requierements.txt
```


Pour exécuter le notebook (après avoir activé l'environnement virtuel `source activate proj_env/bin/activate`) :
```bash
jupyter notebook
```


Pour rendre le notebook en HTML (après avoir activé l'environnement virtuel `source activate proj_env/bin/activate`) :
```bash
jupyter nbconvert --execute --to html_toc analysis.ipynb
```

## Configuration

Les données qui sont utilisées sont définies dans le fichier `config.yaml` (voir les commentaires dans ce fichier pour les explications).

Vous pouvez restreindre l'analyse à une liste d'UVs (voir `restrict_to_dept` dans `config.yaml`).


# Licence

Tous scripts et fichiers de ce repo sont mis à disposition sous licence `BSD 2-Clause`. 

Les résultats de l'intégration continue (qui contiennent les vraies données de l'UTC) ne sont pas rendus publics et seront limités à un usage interne. 
