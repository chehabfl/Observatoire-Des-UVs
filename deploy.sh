#!/bin/sh

rm -rf UVS_evals
mkdir UVS_evals

wget https://gitlab.utc.fr/chehabfl/CAS_UTC_PHP/raw/master/cas.php
wget https://gitlab.utc.fr/chehabfl/CAS_UTC_PHP/raw/master/test_login_display.php

cat test_login_display.php > ./UVS_evals/res.php
cat analysis.html >> ./UVS_evals/res.php

cp ./cas.php ./UVS_evals/cas.php 

sshpass -p "$CAS_PASSWORD" scp -o StrictHostKeyChecking=no -r ./UVS_evals $CAS_LOGIN@stargate.utc.fr:~/public_html/
